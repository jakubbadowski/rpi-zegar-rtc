# rpi-zegar-rtc
Problem z czasem rzeczywistym na RPI. Sposoby na aktualny czas w trybie offline.

## sprawdzanie czasu, strefy czasowej
1. date - aktualny czas
2. cat /etc/timezone - sprawdza ustawienie strefy czasowej np. Europe/Warsaw
3. sudo nano /etc/ntp.conf - ustawienia konfiguracji NTP
4. sudo /etc/init.d/ntp restart - restart NTP

## Konfiguracja 
1. sudo dpkg-reconfigure tzdata - konfiguracja strefy czasowej


## dzia�aj�ca instalacja modu�u DS3231 RTC
### Post�puj zgodnie z fimlami:
1. https://www.youtube.com/watch?v=MthLLRNAGLs - podpi�cie
2. https://www.youtube.com/watch?v=9aN2ocO2AWY - konfiguracja

### Konfiguracja
0. W��czy� interfejs I2C - sudo raspi-congif
1. sudo hwclock -r - odczytanie warto�ci zegara RTC (cannot access...)
2. sudo i2cdetect -y 1
3. sudo vim /etc/modules
- dopisujemy na koniec lini�: rtc-ds3231
4. sudo vim /boot/config.txt
- pod #dtparam=spi=on wpisujemy:
dtoverlay=i2c-rtc,ds3231
5. sudo apt -y remove fake-hwclock - deinstalacja udawanego RTC
6. sudo update-rc.d -f fake-hwclock remove - usuni�cie go z autostartu
7. sudo vim /lib/udev/hwclock-set
- trzeba zakomentowa� pierwszego if-a:
if [ -e /run/systemd/system ]; then
  exit 0
fi
8. sudo reboot
9. sudo hwclock -r - powinna pokaza� si� godzina, a je�li b�dzie nieprawid�owa, to zapisa� z systemu do zegara RTC:
10. sudo hwclock -w
11. date; sudo hwclock -r - sprawdzamy czy daty si� zgadzaj�


## NIeudane pr�by instalacji: 
do sprawdzenia!!!

* sudo apt-get update
* sudo apt-get upgrade
* sudo apt-get update
* sudo apt-get install i2c-tools
* sudo raspi-config -> interfaces -> w��czy� i2c
* sudo i2cdetect -y 1
* sudo modprobe rtc-ds1307
* sudo bash
* echo ds1307 0x68 > /sys/class/i2c-adapter/i2c-1/new_device
* sudo hwclock -w
* sudo hwclock -r
sudo reboot

## Linki
* https://webinsider.pl/raspberry-pi-ntp-network-time-protokol/
* https://geek.net.pl/poradniki/instalujemy-zegar-czasu-rzeczywistego-raspberrypi/
* https://www.youtube.com/watch?v=9aN2ocO2AWY
* https://www.dobreprogramy.pl/cyryllo/Zegar-RTC-w-Raspberry-Pi,61212.html
* http://mikrokontroler.pl/2014/04/03/pierwsze-kroki-z-raspberry-pi-jak-dodac-rtc/

